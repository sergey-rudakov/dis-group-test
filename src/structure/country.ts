export class Country {
    id: number;
    name: string;
    capital: string;
    selected?: boolean;

    constructor() {
        this.selected = false;
    }
}