import { Country } from './structure/country';

export const countries: Country[] = [
    { id: 1, name: 'Russia', capital: 'Moscow' },
    { id: 2, name: 'Germany', capital: 'Berlin' },
    { id: 3, name: 'France', capital: 'Paris' },
];
