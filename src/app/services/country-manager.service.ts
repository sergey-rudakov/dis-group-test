import { Injectable } from '@angular/core';
import { Country } from '../../structure/country';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import * as _ from 'lodash';
import {HttpClient} from '@angular/common/http';

class ResCountries {
    countries: Country[];
}

@Injectable()
export class CountryManagerService {
  private countries: Country[] = [];
  private subject = new Subject<Country[]>();
  private countSelected = new Subject<number>();
  private clearFilter = new Subject<string>();
  private sortField: string;
  private filter: string;
  private url = 'assets/countries.json';

  constructor(private http: HttpClient) { }

  getSubscribe(): Observable<Country[]> {
    return this.subject;
  }

  getCountSelected(): Observable<number> {
      return this.countSelected;
  }

  getClearFiltes(): Observable<string> {
    return this.clearFilter;
  }

  loadCountries(): void {
    this.http.get<ResCountries>( this.url ).subscribe(res => {
        this.countries = res.countries;
        this.subject.next( this.countries );
    });
  }

  filtrate( term: string  ): void {
    if ( term.trim().length > 2 ) {
      this.filter = term.trim();
    } else {
      this.filter = '';
    }
    this.update();
  }

  sort( field: string ): void {
    this.sortField = field;
    this.update();
  }

  applyFilter( countries: Country[], term: string ): Country[] {
      return _.filter( countries, el =>
          el.name.toLowerCase().indexOf( term.toLowerCase() ) >= 0
          || el.capital.toLowerCase().indexOf( term.toLowerCase() ) >= 0 );
  }

  applySort( countries: Country[], field: string ): Country[] {
      return _.sortBy( countries, field );
  }

  update(): void {
    let countries: Country[] = this.countries;

    if ( this.filter ) {
        countries = this.applyFilter( countries, this.filter );
    }

    if ( this.sortField ) {
        countries = this.applySort( countries, this.sortField );
    }

    this.resetSelected( countries );

    this.subject.next( countries );
  }

  resetSelected( countries: Country[] ) {
      for ( const country of this.countries ) {
          if ( countries.indexOf( country ) < 0 ) {
              country.selected = false;
          }
      }

      this.countSelected.next( this.getSelected().length );
  }

  getSelected(): Country[] {
      return _.filter( this.countries, { selected: true } );
  }

  selectById( id: number ): Country {
      return _.find( this.countries, { id: id } );
  }

  selectToggle( id: number ) {
      const el = this.selectById( id );
      el.selected = !el.selected;

      this.countSelected.next( this.getSelected().length );
  }

  lastId(): number {
      const last_id = _.last( this.countries );
      return last_id ? last_id.id + 1 : 1;
  }

  deleteSelected(): void {
    _.remove( this.countries, el => el.selected );
    this.update();
    this.countSelected.next( 0 );
  }

  add( name, capital ): void {
      const new_country = new Country();
      new_country.name = name;
      new_country.capital = capital;
      new_country.id = this.lastId();

      this.clearFilter.next();
      this.countries.push( new_country );

      this.update();
  }
}
