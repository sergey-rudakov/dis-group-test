import {Renderer2, RendererFactory2, Injectable} from '@angular/core';
import {ModalComponent} from '../../components/modal/modal.component';
import * as _ from 'lodash';

@Injectable()
export class ModalService {
  private modals: ModalComponent[] = [];
  private renderer: Renderer2;

  constructor(rendererFactory: RendererFactory2) {
      this.renderer = rendererFactory.createRenderer(null, null);
  }

  flagBody() {
      const modals = _.find( this.modals, { isOpen: true } );
      if ( !modals ) {
          this.renderer.addClass( document.body, 'modal-open' );
      }
  }

  UnFlagBody() {
      const modals = _.find( this.modals, { isOpen: true } );
      if ( !modals ) {
          this.renderer.removeClass( document.body, 'modal-open' );
      }
  }

  add( modal: ModalComponent ) {
    this.modals.push( modal );
  }

  open( id: string ) {
      const modal = _.find(this.modals, { id: id });
      if ( modal ) {
          this.flagBody();
          modal.open();
      }
  }

  close( id: string ) {
      const modal = _.find(this.modals, { id: id });
      if ( modal ) {
          modal.close();
          this.UnFlagBody();
      }
  }
}
