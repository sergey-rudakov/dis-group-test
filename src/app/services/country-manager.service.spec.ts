import { TestBed, inject } from '@angular/core/testing';

import { CointryManagerService } from './country-manager.service';

describe('CointryManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CointryManagerService]
    });
  });

  it('should be created', inject([CointryManagerService], (service: CointryManagerService) => {
    expect(service).toBeTruthy();
  }));
});
