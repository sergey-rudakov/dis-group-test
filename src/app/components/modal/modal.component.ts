import { Component, OnInit, Input } from '@angular/core';
import {ModalService} from '../../services/modal-service/modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  @Input() id: string;
  @Input() modalTitle: string;
  isOpen = false;

  constructor(private modalService: ModalService) { }

  open() {
    this.isOpen = true;
  }

  close() {
    this.isOpen = false;
  }

  closeByService() {
      this.modalService.close( this.id );
  }

  ngOnInit() {
    this.modalService.add( this );
  }

}
