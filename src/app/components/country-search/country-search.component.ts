import { Component, OnInit } from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {CountryManagerService} from '../../services/country-manager.service';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-country-search',
  templateUrl: './country-search.component.html',
  styleUrls: ['./country-search.component.scss']
})
export class CountrySearchComponent implements OnInit {
  private term = new Subject<string>();

  constructor(private countryService: CountryManagerService) { }

  search( term: string ): void {
    this.term.next( term );
  }

  ngOnInit() {
      this.term.pipe(
          debounceTime( 300 ),
          distinctUntilChanged(),
      ).subscribe( term => this.countryService.filtrate( term ) );

      this.countryService.getClearFiltes().subscribe( clear => this.term.next('') );
  }

}
