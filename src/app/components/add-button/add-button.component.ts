import { Component, OnInit } from '@angular/core';
import {ModalService} from '../../services/modal-service/modal.service';

@Component({
  selector: 'app-add-button',
  templateUrl: './add-button.component.html',
  styleUrls: ['./add-button.component.scss']
})
export class AddButtonComponent implements OnInit {

  constructor(private modalService: ModalService) { }

  getModal() {
    this.modalService.open( 'add-country-modal' );
  }

  ngOnInit() {
  }

}
