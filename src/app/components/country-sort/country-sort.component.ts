import { Component, OnInit } from '@angular/core';
import {CountryManagerService} from '../../services/country-manager.service';

@Component({
  selector: 'app-country-sort',
  templateUrl: './country-sort.component.html',
  styleUrls: ['./country-sort.component.scss']
})
export class CountrySortComponent implements OnInit {
  field = '';
  fieldList = [
    { 'value': 'id', 'text': 'Default'},
    { 'value': 'name', 'text': 'Country name'},
    { 'value': 'capital', 'text': 'Capital'},
  ];

  constructor(private countryService: CountryManagerService) { }

  sort( field: string ): void {
    this.field = field;
    this.countryService.sort( this.field );
  }

  ngOnInit() {
  }
}
