import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountrySortComponent } from './country-sort.component';

describe('CountrySortComponent', () => {
  let component: CountrySortComponent;
  let fixture: ComponentFixture<CountrySortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountrySortComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountrySortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
