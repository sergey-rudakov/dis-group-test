import { Component, OnInit } from '@angular/core';
import {CountryManagerService} from '../../services/country-manager.service';

@Component({
  selector: 'app-delete-button',
  templateUrl: './delete-button.component.html',
  styleUrls: ['./delete-button.component.scss']
})
export class DeleteButtonComponent implements OnInit {
  selectedCount: number;

  constructor(private countryService: CountryManagerService) { }

  ngOnInit() {
    this.countryService.getCountSelected().subscribe( count => { this.selectedCount = count; } );
  }

  delete() {
    this.countryService.deleteSelected();
  }

}
