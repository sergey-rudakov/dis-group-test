import { Component, OnInit } from '@angular/core';
import {CountryManagerService} from '../../services/country-manager.service';
import {Country} from '../../../structure/country';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.scss']
})
export class CountryListComponent implements OnInit {
  list: Country[] = [];

  constructor(private countryService: CountryManagerService) {
    this.countryService.getSubscribe().subscribe( list => { this.list = list; } );
  }

  ngOnInit() {
    this.getCountries();
  }

  getCountries() {
    this.countryService.loadCountries();
  }
}
