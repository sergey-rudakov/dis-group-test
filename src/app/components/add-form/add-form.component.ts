import { Component, OnInit } from '@angular/core';
import {ModalService} from '../../services/modal-service/modal.service';
import {CountryManagerService} from '../../services/country-manager.service';

@Component({
  selector: 'app-add-form',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.scss']
})
export class AddFormComponent implements OnInit {
  name: string;
  capital: string;
  isValid = true;

  constructor(private modalService: ModalService, private countryService: CountryManagerService) { }

  cancel() {
    this.modalService.close( 'add-country-modal' );
  }

  validate() {
    this.isValid = !( !this.name && !this.capital );
    return this.isValid;

  }

  ok() {
    if ( this.validate() ) {
      this.countryService.add( this.name, this.capital );
      this.modalService.close( 'add-country-modal' );
    }
  }

  ngOnInit() {
  }

}
