import { Component, OnInit, Input } from '@angular/core';
import {Country} from '../../../structure/country';
import {CountryManagerService} from '../../services/country-manager.service';

@Component({
  selector: 'app-country-item',
  templateUrl: './country-item.component.html',
  styleUrls: ['./country-item.component.scss']
})
export class CountryItemComponent implements OnInit {
  @Input() data: Country;

  constructor(private countryService: CountryManagerService) { }

  selectToggle( selected: boolean ): void {
    this.countryService.selectToggle( this.data.id );
  }

  ngOnInit() {
  }
}
