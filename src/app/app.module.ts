import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CountryManagerService } from './services/country-manager.service';


import { AppComponent } from './app.component';
import { CountryListComponent } from './components/country-list/country-list.component';
import { CountrySearchComponent } from './components/country-search/country-search.component';
import { CountrySortComponent } from './components/country-sort/country-sort.component';
import { CountryItemComponent } from './components/country-item/country-item.component';
import { DeleteButtonComponent } from './components/delete-button/delete-button.component';
import { ModalComponent } from './components/modal/modal.component';
import {ModalService} from './services/modal-service/modal.service';
import { AddButtonComponent } from './components/add-button/add-button.component';
import { AddFormComponent } from './components/add-form/add-form.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    CountryListComponent,
    CountrySearchComponent,
    CountrySortComponent,
    CountryItemComponent,
    DeleteButtonComponent,
    ModalComponent,
    AddButtonComponent,
    AddFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
      CountryManagerService,
      ModalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
